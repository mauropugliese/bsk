package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {
	
	private Game createGame() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(1,5));
		createGame.addFrame(new Frame(2,5));
		createGame.addFrame(new Frame(1,5));
		createGame.addFrame(new Frame(1,1));
		createGame.addFrame(new Frame(4,2));
		createGame.addFrame(new Frame(8,0));
		createGame.addFrame(new Frame(2,3));
		createGame.addFrame(new Frame(1,3));
		createGame.addFrame(new Frame(1,6));
		createGame.addFrame(new Frame(2,9));
		createGame.addFrame(new Frame(10,0));
		return createGame;
		
		
	}

	@Test
	public void testFirstFrameAndFirstThrow() throws BowlingException {
		Game game= createGame();
		
		Frame firstframe = game.getFrameAt(0);
		assertEquals(1,firstframe.getFirstThrow());
		
	}
	@Test
	public void testFirstFrameAndSecondThrow() throws BowlingException {
		Game game= createGame();
		
		Frame firstframe = game.getFrameAt(0);
		assertEquals(5,firstframe.getSecondThrow());
		
	}
	
	@Test
	public void testGameScore() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(1,5));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(2,6));
		
		
		assertEquals(81,createGame.calculateScore());
		
	}
	
	@Test
	public void testGameScoreContainingSpareFrame() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(1,9));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(2,6));
		
		
		assertEquals(88,createGame.calculateScore());
		
	}
	
	@Test
	public void testGameScoreContainingStrikeFrame() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(2,6));
		
		
		assertEquals(94,createGame.calculateScore());
		
	}
	
	@Test
	public void testGameScoreContainingStrikeFrameAndSpareFrame() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(4,6));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(2,6));
		
		
		assertEquals(103,createGame.calculateScore());
		
	}
	
	@Test
	public void testGameScoreContainingMultipleStrikesFrame() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(2,6));
		
		
		assertEquals(112,createGame.calculateScore());
		
	}
	
	@Test
	public void testGameScoreContainingMultipleSparesFrame() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(8,2));
		createGame.addFrame(new Frame(5,5));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(2,6));
		
		
		assertEquals(98,createGame.calculateScore());
		
	}
	
	@Test
	public void testSetAndGetFirstBonusThrow() throws BowlingException{
		Game createGame= new Game();
		createGame.addFrame(new Frame(1,5));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(2,8));
		
		createGame.setFirstBonusThrow(7);
		assertEquals(7,createGame.getFirstBonusThrow());	
	}
	
	@Test
	public void testGameScoreContainingLastSpareFrame() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(1,5));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(2,8));
		createGame.setFirstBonusThrow(7);
		
		assertEquals(90,createGame.calculateScore());
		
	}
	
	@Test
	public void testSetAndGetSecondBonusThrow() throws BowlingException{
		Game createGame= new Game();
		createGame.addFrame(new Frame(1,5));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(2,8));
		
		createGame.setSecondBonusThrow(2);
		assertEquals(2,createGame.getSecondBonusThrow());	
	}
	
	@Test
	public void testGameScoreContainingLastStrikeFrame() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(1,5));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(7,2));
		createGame.addFrame(new Frame(3,6));
		createGame.addFrame(new Frame(4,4));
		createGame.addFrame(new Frame(5,3));
		createGame.addFrame(new Frame(3,3));
		createGame.addFrame(new Frame(4,5));
		createGame.addFrame(new Frame(8,1));
		createGame.addFrame(new Frame(10,0));
		
		createGame.setFirstBonusThrow(7);
		createGame.setSecondBonusThrow(2);
		
		assertEquals(92,createGame.calculateScore());
		
	}
	
	@Test
	public void testGameBestScore() throws BowlingException {
		Game createGame= new Game();
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		createGame.addFrame(new Frame(10,0));
		
		createGame.setFirstBonusThrow(10);
		createGame.setSecondBonusThrow(10);
		
		assertEquals(300,createGame.calculateScore());
		
	}
	
	
}
