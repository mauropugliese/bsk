package bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {
	
	

	@Test
	public void firstThrowTwoPins(){
		Frame frame= new Frame(2,4);
		assertEquals(2,frame.getFirstThrow());
	}


	@Test
	public void secondThrowFourPins(){
		Frame frame= new Frame(2,4);
		assertEquals(4,frame.getSecondThrow());	
	}
	
	@Test
	public void scoreOfTheFrame(){
		Frame frame= new Frame(2,6);
		assertEquals(8,frame.getScore());	
	}
	
	
	@Test
	public void testSetAndGetBonus(){
		Frame frame= new Frame(1,9);
		frame.setBonus(3);
		assertEquals(3,frame.getBonus());	
	}
	
	@Test
	public void testFrameIsSpare(){
		Frame frame= new Frame(1,9);
		
		assertEquals(true,frame.isSpare());	
	}
	
	@Test
	public void testFrameIsStrike(){
		Frame frame= new Frame(10,0);
		
		assertEquals(true,frame.isStrike());	
	}
	
	
	
	
	
}