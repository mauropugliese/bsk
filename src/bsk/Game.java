

package bsk;
import java.util.ArrayList;

public class Game {

	ArrayList<Frame> frames = new ArrayList<>();
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		
		// To be implemented
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		// To be implemented
		return frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		// To be implemented
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int gameScore = 0;
		int firstBonus = 0;
		int secondBonus = 0;
		
		for(int i=0;i<frames.size();i++) {
			
			
			Frame frame= getFrameAt(i);
			if(frame.isSpare()) {
				if(i==frames.size()-1) {
					 firstBonus= getFirstBonusThrow();
				}
				
				else {
					Frame framenext= getFrameAt(i+1);
					frame.setBonus(framenext.getFirstThrow());
				}
				
			}
			
			
			else if(frame.isStrike()) {
				if(i>=frames.size()-2) {
					 firstBonus= getFirstBonusThrow();
					 secondBonus= getSecondBonusThrow();	
				}
				
				else {
					setBonusFrameStrike(frame,i);
				}
			}
			
			
			gameScore= gameScore + frame.getScore() + firstBonus + secondBonus;	
			
			
		}
		
		return gameScore;
			
	}



	private void setBonusFrameStrike(Frame frame, int i) throws BowlingException {
		Frame framenext= getFrameAt(i+1);
		if(framenext.isStrike() && i<frames.size()-2) {
			frame.setBonus(framenext.getFirstThrow() + framenext.getSecondThrow() + getFrameAt(i+2).getFirstThrow());
		}
		else{
			frame.setBonus(framenext.getFirstThrow() + framenext.getSecondThrow());	
		}
	}

}
